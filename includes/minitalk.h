/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minitalk.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/28 12:46:35 by gneve             #+#    #+#             */
/*   Updated: 2021/06/07 15:03:50 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINITALK_H
# define MINITALK_H
# include <stdlib.h>
# include <unistd.h>
# include <signal.h>

# define SIZE 7

# ifndef DELAY
#  define DELAY 250
# endif

void	ft_putnbr(int n, int fd);
void	ft_putchar_fd(char c, int fd);
int		ft_atoi(const char *str);
size_t	ft_strlen(const char *str);

#endif
