/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gneve <gneve@student.s19.be>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/28 12:57:13 by gneve             #+#    #+#             */
/*   Updated: 2021/06/06 10:08:31 by gneve            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minitalk.h"

int	ft_atoi(const char *str)
{
	int			neg;
	int			i;
	long int	num;

	i = 0;
	neg = 1;
	num = 0;
	while ((str && str[i] == ' ') || (str[i] >= 9 && str[i] <= 13))
		i++;
	if (str[i] == '-' || str[i] == '+')
		if (str[i++] == '-')
			neg *= -1;
	while (str[i] >= 48 && str[i] <= 57)
	{
		num = num * 10 + (str[i++] - 48);
		if (num < 0)
		{
			if (neg < 0)
				return (0);
			else
				return (-1);
		}
	}
	return (num * neg);
}
