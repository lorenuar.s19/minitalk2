/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gneve <gneve@student.s19.be>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/28 12:45:16 by gneve             #+#    #+#             */
/*   Updated: 2021/06/06 14:29:18 by gneve            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minitalk.h"
#include <stdio.h>

void	sig_set(int code)
{
	static unsigned char	c = 0;
	static int			 	bitcnt = 0;

	if (code == SIGUSR1)
	{
		c += (1 << (bitcnt));
	}
	bitcnt++;
	if (bitcnt > SIZE)
	{
		ft_putchar_fd(c, 1);
		bitcnt = 0;
		c = 0;
	}
}

static int	setup(void)
{
	if (signal(SIGUSR1, sig_set) == SIG_ERR)
	{
		return (EXIT_FAILURE);
	}
	if (signal(SIGUSR2, sig_set) == SIG_ERR)
	{
		return (EXIT_FAILURE);
	}
	return (0);
}

static void	pid(void)
{
	write(0, "PID of the server ", 19);
	ft_putnbr(getpid(), 1);
	write(0, "\n", 1);
}

int	main(void)
{
	pid();
	if (setup())
	{
		return (EXIT_FAILURE);
	}
	while (1)
	{
		pause();
	}
}
