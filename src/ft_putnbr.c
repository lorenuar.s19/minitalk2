/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gneve <gneve@student.s19.be>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/28 13:02:48 by gneve             #+#    #+#             */
/*   Updated: 2021/06/06 14:28:13 by gneve            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minitalk.h"

void	ft_putchar_fd(char c, int fd)
{
	write(fd, &c, 1);
}

void	ft_putnbr(int n, int fd)
{
	size_t	nn;

	nn = 0;
	if (n < 0)
	{
		ft_putchar_fd('-', fd);
		if (n == -2147483648)
		{
			n = n + 1;
			nn = 1;
		}
		n = n * -1;
	}
	if (n > 9)
	{
		ft_putnbr(n / 10, fd);
		ft_putchar_fd(((n + nn) % 10) + 48, fd);
	}
	else
	{
		ft_putchar_fd((n + nn) + 48, fd);
	}
}
