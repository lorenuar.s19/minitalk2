/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/28 14:38:07 by gneve             #+#    #+#             */
/*   Updated: 2021/06/07 14:25:16 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minitalk.h"
#include <stdio.h>

static void	usage(char *exec)
{
	write(0, "Command \n --------------------------------- \n", 45);
	write(0, exec, ft_strlen(exec));
	write(0, " <pid> <message> \n", 18);
	write(0, "---------------------------------- \n", 36);
}

static void	send_eof(int pid)
{
	int	bitshift;

	bitshift = -1;
	while (++bitshift < 8)
	{
		if (kill(pid, SIGUSR2) == -1)
		{
			write(0, "INVALID PID \n", 13);
			exit(1);
		}
		usleep(DELAY);
	}
}

static void	message(int pid, char *m)
{
	int	i;
	int	bitshift;

	i = 0;
	while (m && m[i])
	{
		bitshift = 0;
		while (bitshift < SIZE + 1)
		{
			if ((((m[i] >> bitshift)) & 1) == 0)
			{
				if (kill(pid, SIGUSR2) == -1)
					exit(1);
			}
			else
			{
				if (kill(pid, SIGUSR1) == -1)
					exit(1);
			}
			usleep(DELAY);
			bitshift++;
		}
		i++;
	}
	send_eof(pid);
}

int	main(int argc, char **argv)
{
	int	pid;

	if (argc != 3)
	{
		usage(argv[0]);
		exit(0);
	}
	pid = ft_atoi(argv[1]);
	if (pid <= 0)
		exit (1);
	message(pid, argv[2]);
	return (0);
}
