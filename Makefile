# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/04/10 13:37:24 by lorenuar          #+#    #+#              #
#    Updated: 2021/06/07 13:56:28 by lorenuar         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

# ================================ VARIABLES ================================= #

# The name of your executable
SERVER_NAME	= server
CLIENT_NAME	= client

ALL_NAME = $(SERVER_NAME) $(CLIENT_NAME)

# Compiler and compiling flags
CC	= gcc
CFLAGS	= -Wall -Werror -Wextra

# Debug, use with`make DEBUG=1`
ifeq ($(DEBUG),1)
CFLAGS	+= -g3 -fsanitize=address
endif

# Folder name
SRCDIR	= src/
INCDIR	= includes/
OBJDIR	= bin/

# Add include folder
CFLAGS	+= -I $(INCDIR)

# Linking stage flags
LDFLAGS =

###▼▼▼<src-updater-do-not-edit-or-remove>▼▼▼
# **************************************************************************** #
# **   Generated with https://github.com/lorenuars19/makefile-src-updater   ** #
# **************************************************************************** #

SER_SRCS =\
	./src/server.c\

CLI_SRCS =\
	./src/client.c\

UTI_SRCS =\
	./src/ft_atoi.c\
	./src/ft_putnbr.c\
	./src/ft_strlen.c\


HEADERS =\
	./includes/minitalk.h\

###▲▲▲<src-updater-do-not-edit-or-remove>▲▲▲

# String manipulation magic
SER_SRC		:= $(notdir $(SER_SRCS))
CLI_SRC		:= $(notdir $(CLI_SRCS))
UTI_SRC		:= $(notdir $(UTI_SRCS))

SER_OBJ		:= $(SER_SRC:.c=.o)
CLI_OBJ		:= $(CLI_SRC:.c=.o)
UTI_OBJ		:= $(UTI_SRC:.c=.o)

SER_OBJS	:= $(addprefix $(OBJDIR), $(SER_OBJ))
CLI_OBJS	:= $(addprefix $(OBJDIR), $(CLI_OBJ))
UTI_OBJS	:= $(addprefix $(OBJDIR), $(UTI_OBJ))

ALL_SRC = $(SER_SRC) $(CLI_SRC) $(UTI_SRC)
ALL_SRCS = $(SER_SRCS) $(CLI_SRCS) $(UTI_SRCS)

ALL_OBJ = $(SER_OBJ) $(CLI_OBJ) $(UTI_OBJ)
ALL_OBJS = $(SER_OBJS) $(CLI_OBJS) $(UTI_OBJS)

# Colors
GR	= \033[32;1m
RE	= \033[31;1m
YE	= \033[33;1m
CY	= \033[36;1m
RC	= \033[0m

# Implicit rules
VPATH := $(SRCDIR) $(OBJDIR) $(shell find $(SRCDIR) -type d)

# ================================== RULES =================================== #

all : $(UTI_OBJS) $(SERVER_NAME) $(CLIENT_NAME)

# Compiling
$(OBJDIR)%.o : %.c
	@mkdir -p $(OBJDIR)
	@printf "$(GR)+$(RC)"
	@$(CC) $(CFLAGS) -c $< -o $@

# Linking
$(SERVER_NAME)	: $(SER_SRCS) $(HEADERS) $(SER_OBJS)
	@printf "\n$(GR)=== Compiled [$(CC) $(CFLAGS)] ===\n--- $(SER_SRC)$(RC)\n"
	@$(CC) $(CFLAGS) $(SER_OBJS) $(UTI_OBJS) $(LDFLAGS) -o $(SERVER_NAME)
	@printf "$(YE)&&& Linked [$(CC) $(LDFLAGS)] &&&\n--- $(SERVER_NAME)$(RC)\n"

$(CLIENT_NAME)	: $(CLI_SRCS) $(HEADERS) $(CLI_OBJS)
	@printf "\n$(GR)=== Compiled [$(CC) $(CFLAGS)] ===\n--- $(CLI_SRC)$(RC)\n"
	@$(CC) $(CFLAGS) $(CLI_OBJS) $(UTI_OBJS) $(LDFLAGS) -o $(CLIENT_NAME)
	@printf "$(YE)&&& Linked [$(CC) $(LDFLAGS)] &&&\n--- $(CLIENT_NAME)$(RC)\n"

# Cleaning
clean :
	@printf "$(RE)--- Removing $(OBJDIR)$(RC)\n"
	@rm -rf $(OBJDIR)

fclean : clean
	@printf "$(RE)--- Removing $(ALL_NAME)$(RC)\n"
	@rm -f $(ALL_NAME)

# Special rule to force to remake everything
re : fclean all

# This runs the program
run : $(NAME)
	@printf "$(CY)>>> Running $(NAME)$(RC)"
	./$(NAME)

# This specifies the rules that does not correspond to any filename
.PHONY	= all run clean fclean re
